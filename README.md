

-- generate private key

openssl genrsa -des3 -out private.pem 2048

-- extract public key

openssl rsa -in private.pem -pubout > mykey.pub


-- verify fingerprint of a key
openssl pkcs8 -in private.pem -inform PEM -outform DER -topk8 -nocrypt | openssl sha1 -c


-- create certificate from private key

openssl req -x509 -key private.pem -passin pass:test -nodes -days 365 -out test.crt -config cert.cnf

hmm alt names are not coming true

default_bits        = 2048
distinguished_name  = dn
x509_extensions     = san
req_extensions      = san
extensions          = san
prompt              = no
[ dn ]
countryName         = US
stateOrProvinceName = Massachusetts
localityName        = Boston
organizationName    = MyCompany
[ san ]
subjectAltName = @alt_names
keyUsage = nonRepudiation, digitalSignature, keyEncipherment, dataEncipherment, keyCertSign, cRLSign
extendedKeyUsage = serverAuth, clientAuth, timeStamping

[alt_names]
DNS.1 = personify.be
DNS.2 = ipersonic.be
DNS.3 = hyperseus.com





-- check crt

openssl x509 -in test.crt -text -noout


