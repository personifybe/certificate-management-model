package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.iam.certificatemanagement.model.certificate.enumerations.SystemPropertyType;
import be.personify.util.generator.MetaInfo;


@Entity
@MetaInfo(group = "certificate", name = "SystemProperty",
        description = "A system property", iconClass = "cog",
        showInMenu = true,
        frontendGroup = "admin",
        number = 9,
        rst_include_description = true,
        rst_include_ui = true
)
@Table(name = "system_property", indexes = {
        @Index(name = "idx_system_property_key", columnList = "kkey"),
        @Index(name = "idx_system_property_value", columnList = "vvalue"),
        @Index(name = "idx_system_property_group", columnList = "ggroup")
})
public class SystemProperty extends Persisted implements Serializable {

    private static final long serialVersionUID = 1797157339855588526L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @MetaInfo(showInSearchResultGrid = false, name = "id", description = "the id of the property")
    private long id;

    @Column(name = "kkey",length = 80)
    @MetaInfo(name = "key", description = "the key of the property")
    private String key;


    @Column(name = "vvalue",length = 120)
    @MetaInfo(name = "value", description = "the value of the property")
    private String value;

    @Enumerated(EnumType.STRING)
    @Column(name = "ttype",length = 25)
    @MetaInfo(showInSearchResultGrid = false, name = "type", description = "the type of the property")
    private SystemPropertyType type;

    @Column(name = "ggroup",length = 80)
    @MetaInfo(showInSearchResultGrid = false, name = "group", description = "the group of the property", required = false)
    private String group;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }


    public void setValue(String value)  {
            this.value=value;
    }

    public SystemPropertyType getType() {
        return type;
    }

    public void setType(SystemPropertyType type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

   


    @Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	

    
    

}
