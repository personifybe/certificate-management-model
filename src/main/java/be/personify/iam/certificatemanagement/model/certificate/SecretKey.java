package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.iam.certificatemanagement.model.certificate.enumerations.Algorythm;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="certificate", frontendGroup="certificate", name="privateKey", description="A private key", 
		showInMenu=false,
		iconClass="key",
		sortOrderInGroup=1, 
		workflowEnabled=false,
		isConcept = false,
		number=1,
		addable=true,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="private_key", indexes = {
		@Index(name = "idx_consent_uniq", columnList = "identity,processingPurpose", unique=true)
})
public class SecretKey extends Persisted implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the secret key", showInSearchResultGrid = false)
	private long id;

	@MetaInfo( name="key", description="The secret key", searchable=true )
	private String key;
	
	@MetaInfo( name="size", description="The size of the secret key", searchable=false, fixedInitialValue = "2048" )
	private int size;
	

	

}
