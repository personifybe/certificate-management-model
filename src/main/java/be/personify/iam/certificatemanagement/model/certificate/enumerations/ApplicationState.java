package be.personify.iam.certificatemanagement.model.certificate.enumerations;

public enum ApplicationState {
	
	ACTIVE,
	FROZEN,
	PLANNED

}
