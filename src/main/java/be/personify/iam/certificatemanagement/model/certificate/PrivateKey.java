package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import be.personify.iam.certificatemanagement.model.certificate.enumerations.Algorythm;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="certificate", frontendGroup="certificate", name="privateKey", description="A private key", 
		showInMenu=true,
		iconClass="key",
		sortOrderInGroup=1, 
		workflowEnabled=false,
		isConcept = false,
		number=1,
		addable=true,
		editable = false,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="private_key", indexes = {
		@Index(name = "idx_consent_uniq", columnList = "identity,processingPurpose", unique=true)
})
public class PrivateKey extends Persisted implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the private key", showInSearchResultGrid = false)
	private long id;
	
	
	
	@MetaInfo( name="code", description="The code of the private key", searchable=true, showInSearchResultGrid = true )
	private String code;
	
	@MetaInfo( name="fingerprint", description="The fingerprint of the private key", searchable=true, creatable = false, editable = false, required = false, showInSearchResultGrid = false )
	private String fingerprint;
	
	@MetaInfo( name="name", description="The name of the private key", searchable=false, showInSearchResultGrid = false, editable = false, addable = false, viewable = false, required = false, creatable = false)
	private String name;

	@MetaInfo( name="key", description="The key of the private key", searchable=false, showInSearchResultGrid = true, creatable = false, editable = false, required = false)
	@Lob
	private byte[] key;
	
	@MetaInfo( name="passPhrase", description="The passphrase of the private key", searchable=false, customRenderer = "password|(?=^.{8,}$)",  secret = true, showInSearchResultGrid = false, viewable = false )
	private String passPhrase;
	
	@MetaInfo( name="algorythm", description="The algorythm of the private key", searchable=false, showInSearchResultGrid = false, editable = false, fixedInitialValue = "RSA" )
	@Enumerated(EnumType.STRING)
	private Algorythm algorythm;
	
	@MetaInfo( name="size", description="The size of the private key", searchable=false, fixedInitialValue = "2048", showInSearchResultGrid = false )
	private int size;
	
	
	
	@MetaInfo( name="generated", description="The generated of the private key", searchable=false, showInSearchResultGrid = false, editable = false, viewable = false, creatable = false )
	private boolean generated;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFingerprint() {
		return fingerprint;
	}

	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}

	public byte[] getKey() {
		return key;
	}

	public void setKey(byte[] key) {
		this.key = key;
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Algorythm getAlgorythm() {
		return algorythm;
	}

	public void setAlgorythm(Algorythm algorythm) {
		this.algorythm = algorythm;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((algorythm == null) ? 0 : algorythm.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((fingerprint == null) ? 0 : fingerprint.hashCode());
		result = prime * result + (generated ? 1231 : 1237);
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((passPhrase == null) ? 0 : passPhrase.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrivateKey other = (PrivateKey) obj;
		if (algorythm != other.algorythm)
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (fingerprint == null) {
			if (other.fingerprint != null)
				return false;
		} else if (!fingerprint.equals(other.fingerprint))
			return false;
		if (generated != other.generated)
			return false;
		if (id != other.id)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (passPhrase == null) {
			if (other.passPhrase != null)
				return false;
		} else if (!passPhrase.equals(other.passPhrase))
			return false;
		if (size != other.size)
			return false;
		return true;
	}
	
	

	
	
	
	
	

}
