package be.personify.iam.certificatemanagement.model.certificate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@MappedSuperclass
public class Persisted implements Serializable {

    public static final long serialVersionUID = 4370674456316231594L;

    @PrePersist
    private void setDates() {
        if (creationDate == null) {
            creationDate = new Date();
        } else {
            modificationDate = new Date();
        }
    }

    @PreUpdate
    private void setUpdateDates() {
        modificationDate = new Date();
    }


    @Column(columnDefinition = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(length =50 )
    private String createdBy;

    @Column(columnDefinition = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    @Column(length =50 )
    private String modifiedBy;


    public Date getCreationDate() {
        return creationDate;
    }

    protected void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    protected void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		return result;
	}

	


}
