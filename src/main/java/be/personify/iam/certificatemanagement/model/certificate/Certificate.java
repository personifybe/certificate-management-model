package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="certificate", frontendGroup="certificate", name="certificate", description="A certificate", 
		showInMenu=true,
		iconClass="key",
		sortOrderInGroup=1, 
		workflowEnabled=false,
		isConcept = false,
		number=1,
		addable=true,
		editable = false,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="certificate", indexes = {
		@Index(name = "idx_consent_uniq", columnList = "identity,processingPurpose", unique=true)
})
public class Certificate extends Persisted implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the certificate", showInSearchResultGrid = false)
	private long id;
	
	@MetaInfo(name="dn", description="The dn of the certificate", showInSearchResultGrid = true)
	private String dn;
	
	@MetaInfo(name="notBefore", description="The notBefore of the certificate", showInSearchResultGrid = false, creatable = false)
	private Date notBefore;
	
	@MetaInfo(name="notAfter", description="The notAfter of the certificate", showInSearchResultGrid = false, creatable = false)
	private Date notAfter;
	
	@MetaInfo(name="subjectAlternateNames", description="The subjectAlternateNames of the certificate", showInSearchResultGrid = false, searchable = false)
	private List<String> subjectAlternateNames;
	
	@MetaInfo(name="privateKey", description="The privateKey of the certificate", showInSearchResultGrid = false, searchable = false)
	private PrivateKey privateKey;
	
	@ManyToOne
	@MetaInfo(name="application", description="The application of the certificate", showInSearchResultGrid = false, searchable = false)
	private Application application; 
	
	@MetaInfo( name="generated", description="The generated of the certificate", searchable=false, showInSearchResultGrid = false, editable = false, viewable = false, creatable = false )
	private boolean generated;
	
	  
    @Column(nullable = false)
    @MetaInfo(name = "daysValid", description = "the daysValid of the certificate", showInSearchResultGrid = false, searchable = false, editable = false, fixedInitialValue = "365")
    private int daysValid;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public List<String> getSubjectAlternateNames() {
		return subjectAlternateNames;
	}

	public void setSubjectAlternateNames(List<String> subjectAlternateNames) {
		this.subjectAlternateNames = subjectAlternateNames;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
	

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public int getDaysValid() {
		return daysValid;
	}

	public void setDaysValid(int daysValid) {
		this.daysValid = daysValid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((application == null) ? 0 : application.hashCode());
		result = prime * result + daysValid;
		result = prime * result + ((dn == null) ? 0 : dn.hashCode());
		result = prime * result + (generated ? 1231 : 1237);
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((notAfter == null) ? 0 : notAfter.hashCode());
		result = prime * result + ((notBefore == null) ? 0 : notBefore.hashCode());
		result = prime * result + ((privateKey == null) ? 0 : privateKey.hashCode());
		result = prime * result + ((subjectAlternateNames == null) ? 0 : subjectAlternateNames.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Certificate other = (Certificate) obj;
		if (application == null) {
			if (other.application != null)
				return false;
		} else if (!application.equals(other.application))
			return false;
		if (daysValid != other.daysValid)
			return false;
		if (dn == null) {
			if (other.dn != null)
				return false;
		} else if (!dn.equals(other.dn))
			return false;
		if (generated != other.generated)
			return false;
		if (id != other.id)
			return false;
		if (notAfter == null) {
			if (other.notAfter != null)
				return false;
		} else if (!notAfter.equals(other.notAfter))
			return false;
		if (notBefore == null) {
			if (other.notBefore != null)
				return false;
		} else if (!notBefore.equals(other.notBefore))
			return false;
		if (privateKey == null) {
			if (other.privateKey != null)
				return false;
		} else if (!privateKey.equals(other.privateKey))
			return false;
		if (subjectAlternateNames == null) {
			if (other.subjectAlternateNames != null)
				return false;
		} else if (!subjectAlternateNames.equals(other.subjectAlternateNames))
			return false;
		return true;
	}

	
	
	
	

	

}
