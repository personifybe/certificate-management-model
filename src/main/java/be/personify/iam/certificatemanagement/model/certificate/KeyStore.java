package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.iam.certificatemanagement.model.certificate.enumerations.KeyStoreType;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="certificate", frontendGroup="certificate", name="keystore", description="A keystore", 
		showInMenu=true,
		iconClass="key",
		sortOrderInGroup=1, 
		workflowEnabled=false,
		isConcept = false,
		number=1,
		addable=true,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="keystore")
public class KeyStore extends Persisted implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the keystore", showInSearchResultGrid = false)
	private long id;

	@MetaInfo(name="name", description="The name of the keystore", showInSearchResultGrid = true)
	private String name;
	
	@MetaInfo(name="password", description="The password of the keystore", showInSearchResultGrid = false, secret = true, searchable = false)
	private String password;
	
	@MetaInfo(name="type", description="The type of the keystore", showInSearchResultGrid = true)
	@Enumerated(EnumType.STRING)
	private KeyStoreType type;
	
	@MetaInfo(name="certificates", description="The certificates of the keystore", showInSearchResultGrid = false)
	private List<Certificate> certificates;
	
	@MetaInfo(name="privateKeys", description="The privateKeys of the keystore", showInSearchResultGrid = false)
	@OneToMany
	private List<PrivateKey> privateKeys;
	
	@MetaInfo(name="secretKeys", description="The secretKeys of the keystore", showInSearchResultGrid = false)
	@OneToMany
	private List<SecretKey> secretKeys;
	
	@MetaInfo(searchable=false, showInSearchResultGrid=true, name = "file", description = "file", viewable = false, editable = false, required = false, addable = false)
	@Lob
	private byte[] file;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public KeyStoreType getType() {
		return type;
	}

	public void setType(KeyStoreType type) {
		this.type = type;
	}

	public List<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}

	public List<PrivateKey> getPrivateKeys() {
		return privateKeys;
	}

	public void setPrivateKeys(List<PrivateKey> privateKeys) {
		this.privateKeys = privateKeys;
	}

	public List<SecretKey> getSecretKeys() {
		return secretKeys;
	}

	public void setSecretKeys(List<SecretKey> secretKeys) {
		this.secretKeys = secretKeys;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((certificates == null) ? 0 : certificates.hashCode());
		result = prime * result + Arrays.hashCode(file);
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((privateKeys == null) ? 0 : privateKeys.hashCode());
		result = prime * result + ((secretKeys == null) ? 0 : secretKeys.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyStore other = (KeyStore) obj;
		if (certificates == null) {
			if (other.certificates != null)
				return false;
		} else if (!certificates.equals(other.certificates))
			return false;
		if (!Arrays.equals(file, other.file))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (privateKeys == null) {
			if (other.privateKeys != null)
				return false;
		} else if (!privateKeys.equals(other.privateKeys))
			return false;
		if (secretKeys == null) {
			if (other.secretKeys != null)
				return false;
		} else if (!secretKeys.equals(other.secretKeys))
			return false;
		if (type != other.type)
			return false;
		return true;
	} 
	
	
	
	
	

	

}
