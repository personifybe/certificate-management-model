package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;


@Entity
@MetaInfo(group = "certificate", name = "Translation", description = "A translation", showInMenu = true,
        iconClass = "comment-alt",
        security = "anonymous",
        number = 10,
        rst_include_description = true,
        rst_include_ui = true,
        rst_diagram_width_percentage = "20",
        frontendGroup = "admin"
)
@Table(name = "translation", indexes = {
        @Index(name = "idx_translation_key_locale", columnList = "kkey,locale", unique = true),
        @Index(name = "idx_translation_value", columnList = "vvalue")
})
public class CertificateTranslation extends Persisted implements Serializable {

    public static final long serialVersionUID = 726554448087105950L;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @MetaInfo(searchable = false, showInSearchResultGrid = false, name = "id", description = "the id of the translation")
    private long id;

    @Column(length = 5)
    @MetaInfo(name = "locale", description = "the locale of the translation")
    private String locale;

    @MetaInfo(name = "key", description = "the key of the translation")
    @Column(name = "kkey",length = 80)
    private String key;

    @MetaInfo(name = "value", description = "the value of the translation")
    @Column(name = "vvalue",length = 120)
    private String value;

    @Column(name = "ttype",length = 80)
    @MetaInfo(showInSearchResultGrid = false, searchable = false, name = "type", description = "the type of the translation")
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	


}
