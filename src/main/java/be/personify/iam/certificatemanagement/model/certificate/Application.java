package be.personify.iam.certificatemanagement.model.certificate;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.iam.certificatemanagement.model.certificate.enumerations.ApplicationState;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo(group = "certificate", name = "application", description = "A application", iconClass = "chalkboard-teacher",
        sortOrderInGroup = 1, showInMenu = true, showOnHomePage = true,
        isConcept = false, number = 1,
        workflowEnabled = false,
        rst_include_description = true,
        frontendGroup = "application",
        rst_include_ui = true
)
@Table(name = "application", indexes = {
        @Index(name = "idx_application_name", columnList = "name"),
        @Index(name = "idx_application_code", columnList = "code")})
public class Application extends Persisted implements Serializable {

    private static final long serialVersionUID = 4205198386311944823L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @MetaInfo(showInSearchResultGrid = false, name = "id", description = "the id of the application")
    private long id;
    
    @Column(nullable = false, length = 80)
    @MetaInfo(name = "code", description = "the code of the application", sampleValue = "IIAMS0001", editable = false)
    private String code;

    @Column(nullable = false, length = 120)
    @MetaInfo(name = "name", description = "the name of the application", sampleValue = "Identity and Access Management", showInSearchResultGridMobile=false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    @MetaInfo(name = "applicationState", description = "the state of the application (active,decommisioned,pending)", showInSearchResultGridMobile=false)
    private ApplicationState applicationState;

    @MetaInfo(name = "description", description = "the description of the application", sampleValue = "Identity and Access Management application",
            showInSearchResultGrid = false, required = false, creatable = false, searchable = false,
            customRenderer = "text_area|rows:3")
    private String description;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "application")
    @ElementCollection(targetClass = Certificate.class)
    @MetaInfo(name = "certificates", description = "the certificates linked to this application")
    private List<Certificate> certificates;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public ApplicationState getApplicationState() {
		return applicationState;
	}


	public void setApplicationState(ApplicationState applicationState) {
		this.applicationState = applicationState;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public List<Certificate> getCertificates() {
		return certificates;
	}


	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((applicationState == null) ? 0 : applicationState.hashCode());
		result = prime * result + ((certificates == null) ? 0 : certificates.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (applicationState != other.applicationState)
			return false;
		if (certificates == null) {
			if (other.certificates != null)
				return false;
		} else if (!certificates.equals(other.certificates))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    
    
    


}
