package be.personify.iam.certificatemanagement.model.certificate.enumerations;

public enum KeyStoreType {

	JKS,JCEKS,P12
	
}
